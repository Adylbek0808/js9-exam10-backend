const fs = require('fs').promises;
const { nanoid } = require('nanoid');

const filename = './database/news.json';
const commentsList = './database/comments.json';

let data = [];
let commentsData = []



module.exports = {
    async init() {
        try {
            const content = await fs.readFile(filename);
            data = JSON.parse(content);
        } catch (error) {
            data = []
        }
    },
    async add(item) {
        if (item.title === "" || item.newsText === "") {
            return ("ERROR. Some fields are empty")
        } else {
            item.id = nanoid();
            item.datetime = new Date().toISOString();
            data.push(item);
            await this.save();
            return item
        }
    },
    async getAll() {
        return data
    },
    async getById(id) {
        return data.find(item => item.id === id)
    },
    async filterComments(id) {
        const comments = await fs.readFile(commentsList)
        commentsData = JSON.parse(comments)
        const filteredList = commentsData.filter(item => item.newsID != id)
        await fs.writeFile(commentsList, JSON.stringify(filteredList, null, 2))
        return filteredList
    },
    async deleteById(id) {
        this.filterComments(id)
        const index = data.findIndex(item => item.id === id);
        data.splice(index, 1);
        await this.save()
    },
    async save() {
        await fs.writeFile(filename, JSON.stringify(data, null, 2))
    }
}