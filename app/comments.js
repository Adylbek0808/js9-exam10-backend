const express = require('express');
const commentsDb = require('../commentsDb');


const router = express.Router();

router.post('/', async (req, res) => {
    const message = await commentsDb.add(req.body);
    res.send(message);
})

router.get('/', async (req, res) => {
    if (Object.keys(req.query).length != 0) {
        const message = await commentsDb.getFilteredComments(req.query.news_id)
        res.send(message)
    } else {
        const message = await commentsDb.getAll();
        res.send(message)
    }
})

router.get('/:id', async (req, res) => {
    const message = await commentsDb.getById(req.params.id)
    res.send(message)
})

router.delete('/:id', async (req, res) => {
    await commentsDb.deleteById(req.params.id);
    res.send(`${req.params.id} Item deleted`)
})




module.exports = router