const express = require('express');
const newsDb = require('../newsDb');



const router = express.Router();

router.post('/', async (req, res) => {
    const message = await newsDb.add(req.body);
    res.send(message);
})

router.get('/', async (req, res) => {
    const message = await newsDb.getAll();
    res.send(message)
})

router.get('/:id', async (req, res) => {
    const message = await newsDb.getById(req.params.id)
    res.send(message)
})

router.delete('/:id', async (req, res) => {
    await newsDb.deleteById(req.params.id);
    res.send(`${req.params.id} Item deleted`)
})



module.exports = router