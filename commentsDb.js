const fs = require('fs').promises;
const { nanoid } = require('nanoid');

const filename = './database/comments.json';

let data = [];

module.exports = {
    async init() {
        try {
            const content = await fs.readFile(filename);
            data = JSON.parse(content);
        } catch (error) {
            data = []
        }
    },
    async add(item) {
        if (item.commentText === "" || item.newsID === "") {
            return ("ERROR. Some fields are empty")
        } else if (item.author === "") {
            item.author = "Anonymous"
        }
        item.id = nanoid();
        item.datetime = new Date().toISOString();
        data.push(item);
        await this.save();
        return item

    },
    async getAll() {
        return data
    },
    async getFilteredComments(id){
        return data.filter(item =>item.newsID === id)
    },
    async getById(id) {
        return data.find(item => item.id === id)
    },
    async deleteById(id) {
        const index = data.findIndex(item => item.id === id);
        data.splice(index, 1);
        await this.save()
    },
    async save() {
        await fs.writeFile(filename, JSON.stringify(data, null, 2))
    }
}