const express = require('express');
const cors = require('cors');

const news = require('./app/news');
const comments = require('./app/comments')

const newsDb = require('./newsDb');
const commentsDb = require('./commentsDb');


const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());


const port = 8000;


app.use('/news', news);
app.use('/comments', comments)

// app.use('/comments', (req, res) => {
//     res.send(req.query)
// })


const run = async () => {
    await newsDb.init();
    await commentsDb.init();
    app.listen(port, () => {
        console.log(`Live at ${port} port`)
    });
}

run().catch(console.error)